from random import randint

#creation of random number

def rand_pos(game,rand_1=1):
    x = []
    y = []
    for i in range (len(game)):
        for j in range(len(game[0])):
            if game[i][j] == 0:
               x.append(i)
               y.append(j)
    if not x:
        return None

    for i in range(rand_1):
        if not x:
            break
        rand_pos = randint(0, len(x)-1)
        game[x[rand_pos]][y[rand_pos]] = 1
        del x[rand_pos]
        del y[rand_pos]
    return game

#Board is created with  0 first

def generate_board(m,n):
    board = [[0 for x in range(n)] for y in range(m)]
    for i  in range (m):
        for j in range(n):
            board[i][j] = 0
    return board

# main logic of code FIBONACCI SERIES

def generate_fib(m,n):
    term = m*n
    fib = []
    fib.append(1)
    fib.append(1)
    #reverse map created
    fib_map = {}
    for i in range(2,term):
        fib.append(fib[i-1] + fib[i-2])
        fib_map[fib[i]] = i
    return {"fib_series":fib,"fib_map":fib_map}

#printing board when called

def print_board(board):
    for i in range(len(board)):
        col = ""
        for j in range(len(board[0])):
            col += str(board[i][j])+ " "
        print (col)
        
#Adding elements to sum_list

def get_sum_list(arr,fib_dict):
    adjelem1 = -1
    sum_list = []
    for elem in arr:
        if elem:
            if adjelem1 == -1:
                adjelem1 = elem
            else:
                if (adjelem1 + elem) in fib_dict["fib_map"]:
                    sum_list.append(adjelem1 + elem)
                    adjelem1 = -1
                else:
                    sum_list.append(adjelem1)
                    adjelem1 = elem

    if adjelem1 != -1:
        sum_list.append(adjelem1)
    return sum_list

#board is updated for every iteration

def update_board(board,strp,upd_list,dir):
    k = 0
    rows = len(board)
    cols = len(board[0])
    upd_len = len(upd_list)
    if dir == "down":
        row = rows-1
        while row>=0:
            if k < upd_len:
                board[row][strp] = upd_list[k]
                k += 1
            else:
                board[row][strp] = 0
            row -=1
    elif dir == "up":
        row = 0
        while row<rows:
            if k < upd_len:
                board[row][strp] = upd_list[k]
                k += 1
            else:
                board[row][strp] = 0
            row+=1
    elif dir == "right":
        col = cols-1
        while col>=0:
            if k < len(upd_list):
                board[strp][col] = upd_list[k]
                k += 1
            else:
                board[strp][col] = 0
            col -=1
    if dir == "left":
        col = 0
        while col<cols:
            if k < len(upd_list):
                board[strp][col] = upd_list[k]
                k += 1
            else:
                board[strp][col] = 0
            col += 1

#direction updation  for every iteration

def mov_dir(dir,board,fib_dict):
    cols = len(board[0])
    rows = len(board)
    if dir == "up" or dir == "down":
        for j in range(cols):
            col_arr = []
            for i in range(rows):
                col_arr.append(board[i][j])
            if dir == "down":
                col_arr.reverse()
            upd_list = get_sum_list(col_arr,fib_dict)
            update_board(board,j,upd_list,dir)

    elif dir == "left" or dir == "right":
        for i in range(rows):
            col_arr=[]
            for j in range(cols):
                col_arr.append(board[i][j])
            if dir == "right":
                col_arr.reverse()
            # print col_arr
            upd_list = get_sum_list(col_arr,fib_dict)
            update_board(board,i,upd_list,dir)

#checking winning condition

def check_win(board,fib_dict):
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j] == fib_dict["fib_series"][-1]:
                return 1
    return 0

#inputs  of the code
board = generate_board(3, 3)
fib_dict = generate_fib(3, 3)
rand_pos(board,2)
print ("Initial board state")
print_board(board)

dir_moves = ["up","down","left","right"]
if check_win(board, fib_dict):
    print ("You won")
    exit()
while True:
    dir = input("Please enter the direction(up/down/left/right). For exiting, enter exit.")
    dir = dir.lower()
    if dir == "exit":
        break
    elif dir in dir_moves:
        mov_dir(dir, board, fib_dict)
    else:
        print ("Please enter a valid move")
        print_board(board)
        continue
    if check_win(board,fib_dict):
        print ("You won")
        break

    if not rand_pos(board):
        print ("Game lost")
        break
    print_board(board)

print_board(board)
print ("Thank you")
